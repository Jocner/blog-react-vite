import React from 'react'
// import '../Styles.css';
import './Header.css';

const Header = () => {
  return (
    <>
     
        <nav className="Nav">

            <div className="Nav-menus">

                <div className="Nav-brand">
                    <li>

                    <a className="Nav-brand-logo" href="/">

                    Instagram

                    </a>
                  </li>  

                </div>

            </div>

        </nav>
      
    </>
  )
}

export default Header
