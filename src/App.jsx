import { useEffect, useState } from 'react';
import './App.css';
import { ArticleList, ButtonList } from './components';
import Header from './components/Header';
import data from './data/data';
import logo from './image/logo.jpeg';
import instagram from './image/instagram.png';
import Swal from 'sweetalert2';
import SweetAlert from 'sweetalert2-react';
import { FaInstagramSquare } from "react-icons/fa";
// import '@sweetalert2/themes/dark/dark.scss';

function App() {

    useEffect( () => {
       alertProduct();
	}, []);

	const alertProduct = () => {
		Swal.fire({
			title: 'Sweet!',
			text: 'Modal with a custom image.',
			imageUrl: 'https://unsplash.it/400/200',
			imageWidth: 400,
			imageHeight: 200,
			imageAlt: 'Custom image',
		  })
	}

	const allCategories = [
		'All',
		...new Set(data.map(article => article.category)),
	];

	const [categories, setCategories] = useState(allCategories);
	const [articles, setArticles] = useState(data);

	const filterCategory = (category) => {
		if (category === 'All'){
			setArticles(data)
			return
		}

		const filteredData = data.filter(article => article.category === category);
		setArticles(filteredData)
	}

	return (
		<>
	
		<span><a href='https://www.instagram.com/todoenfierro.cl/' target="_blank">  <img src={instagram} className='instagram' />  </a></span>
			<div className='title'>
				
	
				<h1>
			
           			<span>Todo en Fierro Blog</span> 
				</h1>

              <img src={logo} className="logo" alt="Vite logo" />
			</div>
			
			<ButtonList categories={categories} filterCategory={filterCategory}/>

			<ArticleList articles={articles}/>
		<footer>
		<span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">Desarrollado By Jocner Patiño™ © 2023 All Rights Reserved. </span>
		</footer>
		</>
	);
}

export default App;

