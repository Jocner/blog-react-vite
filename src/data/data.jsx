import porton from '../image/portones/porton1.jpeg';
import porton2 from '../image/portones/porton2.jpeg';
import porton3 from '../image/portones/porton3.jpeg';
import porton4 from '../image/portones/porton4.jpeg';
import porton5 from '../image/portones/porton5.jpeg';
import porton7 from '../image/portones/porton7.jpeg';
import terraza2 from '../image/terrazas/terraza2.jpeg';
// import terraza4 from '../image/terrazas/terraza4.jpeg';
import terraza5 from '../image/terrazas/terraza5.jpeg';
import terraza12 from '../image/terrazas/terraza12.jpeg';
import terraza14 from '../image/terrazas/terraza14.jpeg';
import cobertizo from '../image/cobertizo/cobertizo.jpg';
import cobertizo1 from '../image/cobertizo/cobertizo1.jpg';
import cobertizo2 from '../image/cobertizo/cobertizo2.jpg';
import cobertizo4 from '../image/cobertizo/cobertizo4.jpg';
import cobertizo5 from '../image/cobertizo/cobertizo5.jpg';
import cobertizo6 from '../image/cobertizo/cobertizo6.jpeg';



const data = [

	{
		id: 1,
		image: porton7,
		title: 'Porton 2',
		category: 'Portones',
		description: 'sdfgsdgdfgdfgfdgd',
		date: 'dfsdfsdf',
		ReadingTime: '5 min read',
	},
	{
		id: 2,
		image: terraza2,
		title: 'terraza',
		category: 'Terrazas',
		description:
			'dfssdfsdfsd',
		date: 'sdfsd',
		ReadingTime: '3 min read',
	},
	{
		id: 3,
		image: terraza5,
		title: 'safasfdsf',
		category: 'Terrazas',
		description:
			'DSFDSFSDFSDFDSFSDFSDF',
		date: 'sdfsdfsd',
		ReadingTime: 'sdfsdfsdfsd',
	},
	{
		id: 4,
		image: porton2,
		title: 'porton6',
		category: 'Portones',
		description:
			'sdfsdfsdfdsf',
		date: 'sdfdsfsd',
		ReadingTime: 'sdfsdf',
	},
	{
		id: 9,
		image: terraza12,
		title: 'sdfsdfsdfsdf',
		category: 'Terrazas',
		description:
			'dfsdfsdfsdfsdfdsfsdfdsfsdfdsdfsdf',
		date: 'dgsdfgdfgfdgdfgfdgfdgfdg',
		ReadingTime: 'dsfsdfdsfdsfsdfsd',
	},
	{
		id: 12,
		image: porton3,
		title: 'sdfsdfsdfsdfdsfsd',
		category: 'Portones',
		description: 'sdfsdfdsfdsfdsfsdfsdfdsfds',
		date: 'dsfdsfsdfsdfdsfsd',
		ReadingTime: 'dsfsdfdsfsdf',
	},
	{
		id: 13,
		image: porton5,
		title: 'dsfdsfdsfdsfsd',
		category: 'Portones',
		description: 'sdfsdfdsfdsf',
		date: 'dsfsdfdsfsd',
		ReadingTime: 'sdfsdfsdfdsf',
	},
	{
		id: 14,
		image: terraza14,
		title: 'dsfsdfsdfsdfsd',
		category: 'Terrazas',
		description:
			'dsfsdfsdfsd',
		date: 'dfsfsdfsdfsdfsd',
		ReadingTime: 'dsfdsfsdfsdf',
	},
	{
		id: 15,
		image: cobertizo,
		title: 'dsfsdfdsfsdfsdfdsfd',
		category: 'Cobertizo',
		description: 'dsfsdfdsfsdfds',
		date: 'dsfsdfsdfsd',
		ReadingTime: 'ssrfdsfdsf',
	},
	{
		id: 16,
		image: cobertizo1,
		title: 'Cobertzo alto',
		category: 'Cobertizo',
		description:
			'dfsdfsdfdsfsdfdsfsd',
		date: 'dsfsdfsdfsd',
		ReadingTime: 'dsfsdfsdfsd',
	},
	{
		id: 17,
		image: cobertizo2,
		title: 'dsfsdfsdfsd',
		category: 'Cobertizo',
		description: 'dsfsdfdsfsdfsdfsdfsdf',
		date: 'dssdsdsdsdsd',
		ReadingTime: 'sdfsdfsdfsd',
	},
	{
		id: 18,
		image: cobertizo4,
		title: 'sdfsdfsdfsdfsdfsd',
		category: 'Cobertizo',
		description:
			'dsfsdfsdfsdfsdffffffffffew',
		date: 'dsdsfdsfsdfsdfd',
		ReadingTime: 'dsfsdfsdfsdf',
	},
	{
		id: 19,
		image: cobertizo5,
		title: 'sadasdddasasdasdasdasdasdasdasd',
		category: 'Cobertizo',
		description: 'dfsdfsdfsdfsdfsdfsdfsdfsdfsd',
		date: 'dsfsdfsdfsdfsdf',
		ReadingTime: 'sdfsdfsdf',
	},
	{
		id: 20,
		image: cobertizo6,
		title: 'dsfsdfsdfdsfsdfsdfdsfdsf',
		category: 'Cobertizo',
		description:
			'dsfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdf',
		date: 'dsfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdf',
		ReadingTime: 'dsfsdfsdfsd',
	},
];

export default data;